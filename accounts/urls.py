from django.urls import path
from accounts.views import login_user, signup, logout_user

urlpatterns = [
    path("login/", login_user, name="login"),
    path("logout/", logout_user, name="logout"),
    path("signup/", signup, name="signup"),
]
